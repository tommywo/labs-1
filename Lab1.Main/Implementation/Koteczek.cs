﻿using Lab1.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1.Implementation
{
    public class Koteczek : IKot
    {
        public string Miaucz()
        {
            return "Miał";
        }

        public string Gatunek()
        {
            return "Kot";
        }
    }
}
