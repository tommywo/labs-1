﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab1.Contract;

namespace Lab1.Implementation
{
    public class Pieseczek : IPies 
    {
        public string Szczekaj()
        {
            return "Szczek";
        }

        public string Gatunek()
        {
            return "Pies";
        }

    }
}
