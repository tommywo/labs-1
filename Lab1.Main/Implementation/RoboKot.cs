﻿using Lab1.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1.Implementation
{
    public class RoboKot : IRobot,IKot
    {


        string IRobot.Gatunek()
        {
            return "Robot, który jest kotem";
        }

        void IRobot.Naprawa()
        {
        }

        string IKot.Miaucz()
        {
            return "Miau-szkrzyp-szkyp-MIAUUUUUUUUUUUUUUUUU";
        }

        string ISsak.Gatunek()
        {
            return "Kot, który jest robotem";
        }

        public string Gatunek()
        {
            return "RoboKot";
        }
    }
}
