﻿using Lab1.Contract;
using Lab1.Implementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace Lab1.Main
{
    public struct LabDescriptor
    {
        #region P1

        public static Type IBase = typeof(Lab1.Contract.ISsak);
        
        public static Type ISub1 = typeof(Lab1.Contract.IKot);
        public static Type Impl1 = typeof(Koteczek);
        
        public static Type ISub2 = typeof(Lab1.Contract.IPies);
        public static Type Impl2 = typeof(Pieseczek);


        public static string baseMethod = "Gatunek";
        public static object[] baseMethodParams = new object[] { };

        public static string sub1Method = "Miaucz";
        public static object[] sub1MethodParams = new object[] { };

        public static string sub2Method = "Szczekaj";
        public static object[] sub2MethodParams = new object[] { };

        #endregion

        #region P2

        public static string collectionFactoryMethod = "Lista";
        public static string collectionConsumerMethod = "Pokaż";

        #endregion

        #region P3

        public static Type IOther = typeof(IRobot);
        public static Type Impl3 = typeof(RoboKot);

        public static string otherCommonMethod = "Gatunek";
        public static object[] otherCommonMethodParams = new object[] { };

        #endregion
    }
}
