﻿using Lab1.Contract;
using Lab1.Implementation;
using System;
using System.Collections.Generic;

namespace Lab1.Main
{
    public class Program
    {
        public static IList<Contract.ISsak> Lista()
        {
            List<ISsak> lista = new List<ISsak>();
            lista.Add(new Pieseczek());
            lista.Add(new Koteczek());

            return lista;
        }

        public static void Pokaż(IList<ISsak> lista)
        {
            foreach (var item in lista)
            {
                Console.WriteLine(item.Gatunek());
            }
        }
        
        static void Main(string[] args)
        {
            RoboKot costam = new RoboKot();
            IRobot I1costam = costam;
            IKot I2costam = costam;
            Console.WriteLine(costam.Gatunek());
            Console.WriteLine("czyli,");
            Console.WriteLine(I1costam.Gatunek());
            Console.WriteLine("albo,");
            Console.WriteLine(I2costam.Gatunek()+"\n\n\n");


            Pokaż(Lista());
            Console.ReadKey();


            
           
        }



    }
}
