﻿using System;
using System.Collections;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit.Framework;
using Assert = NUnit.Framework.Assert;

using Lab1.Main;
using Lab1.Contract;
using Lab1.Implementation;
using PK.Test;

namespace Lab1.Test
{
    [TestFixture]
    [TestClass]
    public class P2_Collection
    {
        #region collectionFactoryMethod

        [Test]
        [TestMethod]
        public void P2__Program_Should_Have_CollectionFactoryMethod()
        {
            Helpers.Should_Have_Method(typeof(Program), LabDescriptor.collectionFactoryMethod);
        }

        [Test]
        [TestMethod]
        public void P2__Program_Should_Have_CollectionFactoryMethod_Which_Returns_Collection_Interface()
        {
            // Arrange
            var method = typeof(Program).GetMethod(LabDescriptor.collectionFactoryMethod);
            var returnType = method.ReturnType;

            // Assert
            Assert.That(returnType, Is.Not.Null);
            Assert.That(returnType.IsInterface);
            Helpers.Should_Implement_Interface(returnType, typeof(IEnumerable));
        }

        [Test]
        [TestMethod]
        public void P2__Program_Should_Have_CollectionFactoryMethod_Which_Returns_Generic_Collection_Of_IBase()
        {
            // Arrange
            var method = typeof(Program).GetMethod(LabDescriptor.collectionFactoryMethod);
            var returnType = method.ReturnType;
            var genericTypes = returnType.GetGenericArguments();

            // Assert
            Assert.That(genericTypes, Is.Not.Null.And.Not.Empty);
            Assert.That(genericTypes, Has.Length.EqualTo(1));
            Assert.That(genericTypes, Has.Member(LabDescriptor.IBase));
        }

        [Test]
        [TestMethod]
        public void P2__Program_Should_Have_CollectionFactoryMethod_Which_Returns_Non_Empty_Collection()
        {
            // Arrange
            var instance = new Program();
            var method = typeof(Program).GetMethod(LabDescriptor.collectionFactoryMethod);

            // Act
            var collection = method.Invoke(instance, null);

            // Assert
            Assert.That(collection, Is.Not.Null.And.Not.Empty);
        }

        [Test]
        [TestMethod]
        public void P2__Program_Should_Have_CollectionFactoryMethod_Which_Returns_Collection_With_Items_Of_Different_Types()
        {
            // Arrange
            var instance = new Program();
            var method = typeof(Program).GetMethod(LabDescriptor.collectionFactoryMethod);

            // Act
            var collection = method.Invoke(instance, null);

            // Assert
            Assert.That(collection, Has.Some.InstanceOf(LabDescriptor.Impl1));
            Assert.That(collection, Has.Some.InstanceOf(LabDescriptor.Impl2));
        }

        #endregion

        #region collectionConsumerMethod

        [Test]
        [TestMethod]
        public void P2__Program_Should_Have_CollectionConsumerMethod()
        {
            Helpers.Should_Have_Method(typeof(Program), LabDescriptor.collectionConsumerMethod);
        }

        [Test]
        [TestMethod]
        public void P2__Program_Should_Have_CollectionConsumerMethod_Which_Takes_Generic_Collection_Parameter()
        {
            // Arrange
            var method = typeof(Program).GetMethod(LabDescriptor.collectionConsumerMethod);
            var parameters = method.GetParameters();

            // Assert
            Assert.That(parameters, Has.Length.EqualTo(1));
            Assert.That(parameters[0].ParameterType.GetInterfaces(), Has.Member(typeof(IEnumerable)));
            Assert.That(parameters[0].ParameterType.GetGenericArguments(), Has.Member(LabDescriptor.IBase));
        }

        [Test]
        [TestMethod]
        public void P2__Program_Should_Have_CollectionConsumerMethod_Which_Ivokes_BaseMethod_On_Each_Item()
        {
            // Arrange
            var instance = new Program();
            var factoryMethod = typeof(Program).GetMethod(LabDescriptor.collectionFactoryMethod);
            var collection = factoryMethod.Invoke(instance, null);
            var clearMethod = collection.GetType().GetMethod("Clear");
            clearMethod.Invoke(collection, null);
            var item1 = Activator.CreateInstance(LabDescriptor.Impl1);
            var counter = new Counter();
            var p1 = ObjectProxyFactory.CreateProxy(item1, new String[] { LabDescriptor.baseMethod },
                new Decoration((target, parms) => (parms[0] as Counter).Add(target), new object[] { counter }),
                null);
            var item2 = Activator.CreateInstance(LabDescriptor.Impl2);
            var p2 = ObjectProxyFactory.CreateProxy(item2, new String[] { LabDescriptor.baseMethod },
                new Decoration((target, parms) => (parms[0] as Counter).Add(target), new object[] { counter }),
                null);
            var addMethod = collection.GetType().GetMethod("Add");
            addMethod.Invoke(collection, new object[] { p1 });
            addMethod.Invoke(collection, new object[] { p2 });
            var consumerMethod = typeof(Program).GetMethod(LabDescriptor.collectionConsumerMethod);

            // Act
            consumerMethod.Invoke(instance, new object[] { collection });

            // Assert
            Assert.That(counter.Get(item1), Is.GreaterThan(0));
            Assert.That(counter.Get(item2), Is.GreaterThan(0));
        }

        #endregion
    }
}


